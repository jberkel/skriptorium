#!/usr/bin/env python3

import unittest
import datetime
from skriptorium import pick_font, tool_name, create_image_description, \
  field_image_generation, create_file_description


class SkriptoriumTest(unittest.TestCase):

    def test_pick_font_got(self):
        self.assertEqual('Silubr', pick_font(None, 'got'))

    def test_pick_font_syc(self):
        self.assertEqual('East Syriac Adiabene', pick_font(None, 'syc'))

    def test_pick_font_user(self):
        self.assertEqual('User', pick_font('User', None))

    def test_pick_font_default(self):
        self.assertEqual('Noto Sans', pick_font(None, None))

    def test_field_image_generation(self):
        self.assertEqual('{{Igen|O|n|+|name=skriptorium|code=skriptorium 1 2}}',
                         field_image_generation(cli_arguments=['1', '2']))

    def test_tool_name(self):
        self.assertEqual('skriptorium', tool_name())

    def test_create_file_description(self):
        self.assertEqual("""=={{int:filedesc}}==
{{Information
|description={{en|1=testing}}
|date=2001-01-01
|source={{own}}
|author=[[User:testuser|testuser]]
|permission=
|other versions=
|other fields={{Igen|O|n|+|name=skriptorium|code=skriptorium --foo bar}}
}}

=={{int:license-header}}==
{{self|cc-by-sa-4.0}}""", create_file_description(description='testing',
                                                  user='testuser',
                                                  cli_arguments=['--foo', 'bar'],
                                                  date=datetime.date(2001, 1, 1)))

    def test_create_image_description_syc(self):
        self.assertEqual("The Syriac word [[wikt:en:Text|Text]] (transliteration, \"gloss\")",
                         create_image_description('syc', 'Text', 'transliteration', 'gloss'))

    def test_create_image_description_got(self):
        self.assertEqual("The Gothic word [[wikt:en:Text|Text]] (transliteration, \"gloss\")",
                         create_image_description('got', 'Text', 'transliteration', 'gloss'))


if __name__ == '__main__':
    unittest.main()
