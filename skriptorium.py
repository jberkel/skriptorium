#!/usr/bin/env python3

import datetime
import os
import sys
import pathlib
import argparse
import pango_writer
import mwclient

DEFAULT_FONT_SIZE = 72
DEFAULT_FONT = 'Noto Sans'
PREFERRED_FONTS = {
    'got': 'Silubr',
    'uga': 'MPH 2B Damase',
    'syc': 'East Syriac Adiabene',
    'psu': 'Noto Sans Brahmi',
    'pgl': 'Noto Sans Ogham',
    'ae': 'Noto Sans Avestan',
    'gmy': 'Noto Sans Linear B',
    'peo': 'Noto Sans Old Persian',
    'cop': 'Noto Sans Coptic',
    'gmq-pro': 'Noto Sans Runic',
    'xng': 'Mongolian White',
    'sux': 'Noto Sans Cuneiform',
    'pal': 'Shapour',
    'lep': 'Noto Sans Lepcha',
    'otk': 'Noto Sans Old Turkic',
    'xpu': 'Noto Sans Phoenician'
}
CODE_TO_LANGUAGE = {
    'syc': 'Syriac',
    'uga': 'Ugaritic',
    'got': 'Gothic',
    'psu': 'Sauraseni Prakrit',
    'pgl': 'Primitive Irish',
    'ae': 'Avestan',
    'gmy': 'Mycenaean Greek',
    'peo': 'Old Persian',
    'cop': 'Coptic',
    'gmq-pro': 'Proto-Norse',
    'xng': 'Middle Mongolian',
    'sux': 'Sumerian',
    'pal': 'Middle Persian',
    'lep': 'Lepcha',
    'otk': 'Old Turkic',
    'xpu': 'Punic'
}


def create_image_description(language, text, transliteration, gloss):
    return "The %s word [[wikt:en:%s|%s]] (%s, \"%s\")" % (CODE_TO_LANGUAGE[language], text, text, transliteration, gloss)


def pick_font(specified_font, language):
    if specified_font:
        return specified_font
    elif language in PREFERRED_FONTS:
        return PREFERRED_FONTS[language]
    else:
        return DEFAULT_FONT


def tool_name():
    return pathlib.Path(os.path.basename(__file__)).stem


# https://commons.wikimedia.org/wiki/Template:Image_generation
def field_image_generation(cli_arguments):
    # O = Other tool
    # n = non-SVG
    # + = a "+" (plus sign) when used in |Other fields|
    # code = display of code used for generation
    return "{{Igen|O|n|+|name=%s|code=%s}}" % (
        tool_name(), ' '.join([tool_name()] + cli_arguments))


def create_file_description(description, user, cli_arguments, date=datetime.date.today()):
    return u"""=={{int:filedesc}}==
{{Information
|description={{en|1=%s}}
|date=%s
|source={{own}}
|author=[[User:%s|%s]]
|permission=
|other versions=
|other fields=%s
}}

=={{int:license-header}}==
{{self|cc-by-sa-4.0}}""" % (description, date.isoformat(), user, user, field_image_generation(cli_arguments))


def upload(username, password, filename, file_description):
    from pathlib import Path
    if not password:
        raise Exception('Set MW_PASSWORD first.')

    path = Path(filename)
    assert path.exists
    assert path.stat().st_size > 0

    site = mwclient.Site('commons.wikimedia.org')
    site.login(username, password)

    with open(filename, 'rb') as f:
        response = site.upload(f, filename, file_description, ignore=True,
                           comment='Uploaded with %s' % tool_name())
        result = response.get('Result', None)
        if result == 'Success':
            title = result['canonicaltitle']
            filename = result['filename']
            return '🔗 [[%s|thumb|%s]]' % (title, filename)
        else:
            raise Exception('Error uploading file: %s' % response)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate an image and upload to commons.wikimedia.org.')
    parser.add_argument('--text', dest='text',
                        help='text to render', required=True)
    parser.add_argument('--language', dest='language',
                        help='language code used', default='en', required=False)
    parser.add_argument('--transliteration', dest='transliteration',
                        help='transliteration of text', required=False)
    parser.add_argument('--gloss', dest='gloss',
                        help='gloss of text', required=False)
    parser.add_argument('--user', dest='user',
                        help='mediawiki user name', required=False)
    parser.add_argument('--font', dest='font',
                        help='override font selection', required=False)
    parser.add_argument('--font-size', dest='font_size', type=int,
                        help='set font size (default=%d)' % DEFAULT_FONT_SIZE, default=DEFAULT_FONT_SIZE, required=False)
    parser.add_argument('--reverse', dest='reverse', required=False,
                        help='reverse letters', action='store_true')
    args = parser.parse_args()

    filename = 'Text-%s-%s' % (args.language,
                               args.transliteration or args.text)
    used_font = pick_font(args.font, args.language)
    text = args.text[::-1] if args.reverse else args.text
    (png, svg) = pango_writer.render(text, filename, used_font, args.font_size)
    print('☛ %s\n☛ %s' % (png, svg))

    if args.user:
        image_description = create_image_description(
            args.language, args.text, args.transliteration, args.gloss)
        cli_arguments = sys.argv[1:] + ['--font', used_font]
        file_description = create_file_description(
            image_description, args.user, cli_arguments)
        for file in (svg,):
            print(upload(args.user, os.environ.get(
                'MW_PASSWORD'), file, file_description))
