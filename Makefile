default: lint test

test:
	python3 -m unittest

lint:
	python3 -m pyflakes *.py

