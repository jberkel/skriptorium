Automatically create images from text in unusual scripts, for the [FWOTD][] on [Wiktionary][].

## Installation

You'll need Python3, [cairo][] and [libffi][]:

OSX:

```
$ brew install cairo gobject-introspection
```

Debian/Ubuntu:

```
$ apt-get install libcairo-gobject2 gobject-introspection libgirepository1.0-dev
```

Install Python dependencies:

```
$ pip install -r requirements.txt
```


## Usage

```bash
$ ./skriptorium.py --text "𐍄𐌹𐌿𐌷𐌰𐌽" --language got
Created Text-got-𐍄𐌹𐌿𐌷𐌰𐌽.png
```

To upload to commons.wikimedia.org:

```bash
export MW_PASSWORD=...
$ ./skriptorium.py --text "𐍄𐌹𐌿𐌷𐌰𐌽" --language got --transliteration tiuhan --gloss 'to pull' --user <username>
```

[cairo]: [https://www.cairographics.org]
[Wiktionary]: https://en.wiktionary.org/
[FWOTD]: https://en.wiktionary.org/wiki/Wiktionary:Foreign_Word_of_the_Day
