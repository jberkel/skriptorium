#!/usr/bin/env python3

import unittest
from pathlib import Path
import tempfile
from pango_writer import render, installed_fonts
from PIL import Image


class PangoWriterTest(unittest.TestCase):

    def assert_valid_image(self, file):
        with Image.open(file, mode='r') as image:
            (width, height) = image.size
            self.assertGreater(image.width, 0)
            self.assertGreater(image.height, 0)

    def test_render_image(self):
        filename = tempfile.tempdir + '/test'
        (png, svg) = render('testing', filename, 'Noto Sans', font_size=72)
        for path in [Path(f) for f in [png, svg]]:
            self.assertTrue(path.exists)
            self.assertTrue(path.stat().st_size > 0)
        self.assert_valid_image(png)

    def test_render_gothic(self):
        with tempfile.NamedTemporaryFile() as file:
            (png, _) = render('𐌸', file.name, 'MPH 2B Damase', font_size=72)
            with Image.open(png, mode='r') as image:
                (width, height) = image.size
                self.assertEqual(81, width)
                self.assertEqual(92, height)

    def test_has_fonts_installed(self):
        fonts = installed_fonts()
        self.assertGreater(len(fonts), 0)

    def test_installed_damase(self):
        self.assertTrue('MPH 2B Damase' in installed_fonts())

    def test_installed_syriac_adiabene(self):
        self.assertTrue('East Syriac Adiabene' in installed_fonts())

    def test_installed_noto_sans_avestan(self):
        self.assertTrue('Noto Sans Avestan' in installed_fonts())


if __name__ == '__main__':
    unittest.main()
