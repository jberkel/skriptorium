#!/usr/bin/env python3

import os
os.environ['PANGOCAIRO_BACKEND'] = 'fontconfig'
os.environ['FONTCONFIG_FILE'] = os.path.dirname(
    os.path.realpath(__file__)) + '/fontconfig.xml'
import math
import cairo
import gi
gi.require_version('PangoCairo', '1.0')
from gi.repository import PangoCairo
from gi.repository import Pango


def render(text, filename, font_descriptor, font_size):
    def create_layout(context):
        pangocairo_context = PangoCairo.create_context(context)
        # pangocairo_context.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
        layout = Pango.Layout.new(pangocairo_context)
        font = Pango.FontDescription.from_string(font_descriptor)
        validate_font_descriptor(font)
        font.set_size(font_size * Pango.SCALE)
        layout.set_font_description(font)

        # HACK: get attributes with font-fallback disabled via Pango.parse_markup()
        # workaround for https://bugzilla.gnome.org/show_bug.cgi?id=646788
        (success, attributes, _, _) = Pango.parse_markup(
            '<span fallback="false">%s</span>' % text, -1, '0')
        if not success:
            raise Exception('could not parse text')

        layout.set_text(text, -1)
        layout.set_attributes(attributes)
        context.set_source_rgb(0, 0, 0)
        PangoCairo.update_layout(context, layout)
        PangoCairo.show_layout(context, layout)
        return layout

    initial_width = 1000
    initial_height = 1000
    svg_file = filename + '.svg'
    surf = cairo.SVGSurface(filename + '_temp.svg', initial_width, initial_height)
    context = cairo.Context(surf)
    # fill alpha background
    context.rectangle(0, 0, initial_width, initial_height)
    context.set_source_rgba(0, 0, 0, 0)
    context.fill()

    layout = create_layout(context)
    ink_rect, logical_rect = layout.get_extents()

    width = ink_rect.width
    height = ink_rect.height

    width_px = math.ceil(width / Pango.SCALE)
    height_px = math.ceil(height / Pango.SCALE)

    new_surf = cairo.ImageSurface(cairo.FORMAT_ARGB32, width_px, height_px)
    new_context = cairo.Context(new_surf)
    new_context.set_source_surface(
        surf, - (ink_rect.x / Pango.SCALE), - (ink_rect.y / Pango.SCALE))
    new_context.paint()

    svg_width = math.ceil(logical_rect.width / Pango.SCALE)
    svg_height = math.ceil(logical_rect.height / Pango.SCALE)
    svg_surf = cairo.SVGSurface(svg_file, svg_width, svg_height + 10)
    svg_ctxt = cairo.Context(svg_surf)
    create_layout(svg_ctxt)

    png_file = filename + '.png'
    with open(png_file, 'wb') as image_file:
        new_surf.write_to_png(image_file)

    surf.finish()

    return (png_file, svg_file)


def validate_font_descriptor(descriptor):
    font_map = PangoCairo.FontMap.get_default()
    font = font_map.load_font(font_map.create_context(), descriptor)
    if font:
        description = font.describe()
        if description.get_family() != descriptor.get_family():
            raise Exception("Different font loaded: %s <> %s" %
                            (description.get_family(), descriptor.get_family()))
    else:
        raise Exception("Font %s could not be loaded" % descriptor)


def installed_fonts():
    font_map = PangoCairo.FontMap.get_default()
    families = font_map.list_families()

    return sorted([f.get_name() for f in families])
